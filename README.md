# README #

A docker image that does simple env variable substitution, best used with a compose file, example imcluded in this repo.


###Example to use with the current envfile:

```
docker-compose run envsubsterfile envsubst < testvars.txt > testvarsnew.txt
```

###Or to run with exported env variables


```
export FOXCOLOUR='red' && export DOGCOLOUR='brown'

docker-compose run envsubster envsubst < testvars.txt > testvarsnew.txt
```

###On Dockerhub

https://hub.docker.com/r/joshhatfield/envsubster/ 